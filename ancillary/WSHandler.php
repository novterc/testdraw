<?php

namespace app\ancillary;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use app\ancillary\MessageHandler;

class WSHandler implements MessageComponentInterface {
    public $clients = [];


    public function __construct() {
    }


    public function onOpen( ConnectionInterface $conn )
    {
    	$this->clients[ $conn->resourceId ] = $conn;
        echo "New connection! ({$conn->resourceId})\n";
        $messageHandler = new MessageHandler( $this, $conn );
        $messageHandler->newConnection();
    }
    

    public function onMessage( ConnectionInterface $from, $msg )
    {
        echo sprintf('Connection %d sending message "%s"'."\n", $from->resourceId, '' );
        $messageHandler = new MessageHandler( $this, $from );
        $messageHandler->msg( $msg );
        // $this->sendMessagesAllButOne( $from->resourceId, [ 2, 5 ] );
    }


    public function sendMessageByConnectId( $connId, $msg )
    {
        $client = $this->clients[ $connId ];
        $jsonData = json_encode( $msg );
        $client->send( $jsonData );
        echo 'send;'. "\n";
    }


    public function sendMessagesAllButOne( $oneId, $msg )
    {
    	$clients = $this->clients;
    	unset( $clients[$oneId ] );

    	foreach ( $clients as $clientId => $client ) {
    		$this->sendMessageByConnectId( $clientId, $msg );
    	}
    }


    public function sendMessagesAll( $msg )
    {
    	$clients = $this->clients;
    	foreach ( $clients as $clientId => $client ) {
    		$this->sendMessageByConnectId( $clientId, $msg );
    	}
    }


    public function onClose( ConnectionInterface $conn ) 
    {
        unset( $this->clients[ $conn->resourceId ] );
        echo "Connection {$conn->resourceId} has disconnected\n";
    }


    public function onError( ConnectionInterface $conn, \Exception $e ) 
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }


}
