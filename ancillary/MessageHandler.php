<?php

namespace app\ancillary;

use Ratchet\ConnectionInterface;

Class MessageHandler {
	protected static $_draws = [];
	protected static $_methodsName = [
		'draw' => 'draw',
	];
	protected $_wsHandler;
	protected $_from;
	protected $_json;


	public function __construct( WSHandler &$wsHandler, ConnectionInterface $from )
	{
		$this->_wsHandler = &$wsHandler;
		$this->_from = $from;
	}


	public function msg( $msg )
	{
		$json = $this->basicValidationAndConverting( $msg );
		if( $json !== false ){
			$res = $this->findMethodAndCall( $json->method, $json->data );
		}
	}


	public function newConnection()
	{
		$msg = [
			'method' => 'listDraw',
			'data' => self::$_draws,
		];
		$this->_wsHandler->sendMessageByConnectId( $this->_from->resourceId, $msg );
	}


	protected function basicValidationAndConverting( $msg )
	{
        $json = json_decode( $msg );
        if( $json === false || is_object( $json ) === false  ) return false;
        if( empty( $json->method ) || is_string( $json->method ) === false ) return false;
        if( empty( $json->data ) || is_object( $json->data ) === false ) return false;
        return $json;
	}


	protected function findMethodAndCall( $method, $data )
	{
		if( empty( static::$_methodsName[ $method ] ) ) return false;
		$method = static::$_methodsName[ $method ];
		return $this->$method( $data );
	}


	protected function draw( $data ) 
	{
		$clearData = $this->drawValidationAndFiltration( $data );
		if( $clearData === false )
			return false;
		$this->addDrawAndClearing( $clearData );
		$msg = [
			'method' => 'addDraw',
			'data' => $clearData,
		];
		$this->_wsHandler->sendMessagesAllButOne( $this->_from->resourceId, $msg );
	}


	protected function addDrawAndClearing( $data )
	{
		static::$_draws[] = $data;
		if( count( static::$_draws ) > 10000 ){
			$key = key( static::$_draws );
			$delItem = static::$_draws[ $key ];
			unset( static::$_draws[ $key ] );

			$delItem['color'] = '#ffffff';
			$delItem['width'] = 2;
			$msg = [
				'method' => 'addDraw',
				'data' => $delItem,
			];
			$this->_wsHandler->sendMessagesAll( $msg );
		}

	}


	protected function drawValidationAndFiltration( $data ) 
	{
        if( empty( $data->prev ) || is_object( $data->prev ) === false ) return false;
        if( empty( $data->prev->x ) || is_integer( $data->prev->x ) === false ) return false;
        if( empty( $data->prev->y ) || is_integer( $data->prev->y ) === false ) return false;

        if( empty( $data->curr ) || is_object( $data->curr ) === false ) return false;
        if( empty( $data->curr->x ) || is_integer( $data->curr->x ) === false ) return false;
        if( empty( $data->curr->y ) || is_integer( $data->curr->y ) === false ) return false;

        if( empty( $data->width ) || is_integer( $data->width ) === false ) return false;

        if( empty( $data->color ) || is_string( $data->color ) === false ) return false;
		$pattern = '/^#[0-9abcdef]{3,6}$/';
		$colorValid = preg_match( $pattern, $data->color );
		if( $colorValid === 0 ) return false;

        return [
        	'prev' => [ 'x' => $data->prev->x, 'y' => $data->prev->y ],
        	'curr' => [ 'x' => $data->curr->x, 'y' => $data->curr->y ],
        	'color' => $data->color,
        	'width' => $data->width,
        ];
	}


}
