
        var canvas, ctx, flag = false,
            prevX = 0,
            currX = 0,
            prevY = 0,
            currY = 0,
            dot_flag = false;

        var color = "#000000",
            width = 1;
        
        function init() {
            canvas = document.getElementById('can');
            ctx = canvas.getContext("2d");
            w = canvas.width;
            h = canvas.height;
        
            canvas.addEventListener("mousemove", function (e) {
                findxy('move', e)
            }, false);
            canvas.addEventListener("mousedown", function (e) {
                findxy('down', e)
            }, false);
            canvas.addEventListener("mouseup", function (e) {
                findxy('up', e)
            }, false);
            canvas.addEventListener("mouseout", function (e) {
                findxy('out', e)
            }, false);
        }
        
        function changeColor(value) {
            color = value;
            if (color == "#ffffff") width = 50;
            else width = 1;
        
        }
        
        function erase() {
            var m = confirm("Want to clear");
            if (m) {
                ctx.clearRect(0, 0, w, h);
                document.getElementById("canvasimg").style.display = "none";
            }
        }
        
        function save() {
            document.getElementById("canvasimg").style.border = "2px solid";
            var dataURL = canvas.toDataURL();
            document.getElementById("canvasimg").src = dataURL;
            document.getElementById("canvasimg").style.display = "inline";
        }
        
        function findxy(res, e) {

            if (res == 'down') {
                prevX = (currX+1);
                prevY = (currY+1);
                currX = e.clientX - canvas.offsetLeft;
                currY = e.clientY - canvas.offsetTop;
        
                flag = true;
                dot_flag = true;
            }

            if (res == 'up' || res == "out") {
                flag = false;
            }
            
            if (res == 'move') {
                if (flag) {
                    prevX = currX;
                    prevY = currY;
                    currX = e.clientX - canvas.offsetLeft;
                    currY = e.clientY - canvas.offsetTop;

                    var sendData = { 
                        prev: { x: prevX, y: prevY }, 
                        curr: { x: currX, y: currY },
                        color: color, 
                        width: width,
                    };

                    websocket.send({
                        method:'draw',
                        data: sendData,
                    });

                    draw( sendData );
                }
            }
        }
        
        function draw( data ) {
            ctx.beginPath();
            ctx.moveTo( data.prev.x, data.prev.y );
            ctx.lineTo( data.curr.x, data.curr.y );
            ctx.strokeStyle = data.color;
            ctx.lineWidth = data.width;
            ctx.stroke();
            ctx.closePath();
        }


        var websocket = new websocketClass();
        websocket.connecting();
