var websocketClass = function (config) {
    this.Obj = {

        config: {
            port: "9090",
            uri: window.location.hostname,
        },

        conn: false,
        connectedStatus: false,
        index: 0,
        isdieconnected: false,


        connecting: function () {
            self.conn = new WebSocket('ws://' + self.config.uri + ':' + self.config.port);
            self.conn.onopen = self.onOpen;
            self.conn.onclose = self.onClose;
            self.conn.onmessage = self.onMessage;
            self.conn.onerror = self.onError;
        },


        onOpen: function (event) {
            console.log('webSocket connect open');
            self.connectedStatus = true;
            self.callbeck('onOpen', event);
            self.isdieconnected = true;
        },


        getStat: function(){
            return self.connectedStatus;
        },


        onClose: function (event) {
            console.log('webSocket connect close');
            self.connectedStatus = false;
            self.callbeck('onClose', event);
            if(self.isdieconnected)
                location.reload();
            else
                self.connectTimeOut = setTimeout(self.connecting, 1000);
        },


        onMessage: function (event) {
            console.log('webSocket message', event);
            self.callbeck('onMessage', event);
            if(event.type == "message"){
                var jsonData = JSON.parse(event.data);

                console.log('webSocket message json', jsonData);
                if( jsonData.method != undefined && jsonData.data != undefined  ){
                    self[jsonData.method]( jsonData.data );
                }
            }
        },


        onError: function(event){
            console.log('webSocket connect error');
        },


        send: function(msgObj, requestId){
            if( requestId == undefined ) requestId = self.getRequestId();
            msgObj.requestId = requestId;

            if(self.getStat()==true) {
                self.conn.send(JSON.stringify(msgObj));
            }else{
                self.addCallBack('onOpen',function(){
                    self.conn.send(JSON.stringify(msgObj));
                }, true)
            }
            return msgObj.requestId;
        },


        getRequestId: function(){
            self.index++;
            return new Date().getTime()+'.'+self.index;
        },


        setTest: function(data){
            self.callbeck('setTest', data);
            console.log(data);
        },

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
        addCallBack: function(method, Fn, oneRun){
            if( self.callbacks[method] == undefined )
                self.callbacks[method] = new Array();

            if(oneRun==undefined)oneRun = false;

            return ( self.callbacks[method].push({ callback: Fn, oneRun: oneRun}) -1 );
        },


        delCallBack: function(method, num){
            self.callbacks[method][num] = false;
        },

        callbacks: {},
        callbeck: function(method, data, requestId){
            //console.log('webSocket CallBack method -'+method);
            if( self.callbacks[method] != undefined ){
                self.callbacks[method].forEach(function(item, index) {
                    if(item != false){
                        item.callback(data, requestId);
                        if (item.oneRun == true) {
                            self.callbacks[method][index] = false;
                        }
                    }
                });
            }
        },


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        addDraw: function( data ){
            draw( data )
        },

        listDraw: function( data ){
            for (var key in data) {
                var item = data[key];
                draw( item );
            }
        },


    };

    var self = this.Obj;
    return this.Obj;
};